To build this, you need:
- LaTeX
- pandoc
- Python `pandocfilters` module
- Inkscape (to convert SVG to PDF to be able to inject vector graphics into LaTeX)

To build, run `./build.sh`

Demo SVG file is random file from GitHub: https://github.com/specialorange/FDXCM/blob/master/doc/models_brief.svg
